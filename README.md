Цифровой аналог игры "Memory". 
Миссия игры - развить быструю память игроков.

Описание игры: 
Задача игрока переворачивать по 2 карточки, если карточки совпадут, то они остаются открытыми до конца игры. Если карточки не совпали, то они снова закрываются. Игрок побеждает, когда все карточки станут открытыми.